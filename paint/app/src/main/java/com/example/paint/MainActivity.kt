package com.example.paint

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.os.SystemClock.sleep
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.brush_chooser.*
import java.util.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var brushDialog: Dialog
        var smallBrush = 0F
        var mediumBrush = 0F
        var largeBrush = 0F
        val drawBoard: DrawBoard = drawing
        val paintLayout = paint_colors
        drawBoard.setBrushSize(mediumBrush)
        smallBrush = resources.getInteger(R.integer.small_size).toFloat()
        mediumBrush = resources.getInteger(R.integer.medium_size).toFloat()
        largeBrush = resources.getInteger(R.integer.large_size).toFloat()
        var currPaint = paintLayout.getChildAt(0) as ImageButton
        currPaint.setImageDrawable(resources.getDrawable(R.drawable.paint_pressed))

        new_btn.setOnClickListener() {
            val newDialog: AlertDialog.Builder = AlertDialog.Builder(this)
            newDialog.setTitle("New drawing")
            newDialog.setMessage("Start new drawing (you will lose the current drawing)?")
            newDialog.setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                drawBoard.startNew()
                dialog.dismiss()
            })
            newDialog.setNegativeButton("Cancel",
                DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })
            newDialog.show()
        }

        save_btn.setOnClickListener() {
            val saveDialog = AlertDialog.Builder(this)
            saveDialog.setTitle("Save drawing")
            saveDialog.setMessage("Save drawing to device Gallery?")
            saveDialog.setPositiveButton("Yes") { dialog, which ->
                drawBoard.isDrawingCacheEnabled = true
                val imgSaved: String = MediaStore.Images.Media.insertImage(
                    contentResolver,
                    drawBoard.getDrawingCache(),
                    UUID.randomUUID().toString().toString() + ".png",
                    "drawing"
                )
                if (imgSaved != null) {
                    val savedToast = Toast.makeText(
                        applicationContext,
                        "Drawing saved to Gallery!", Toast.LENGTH_SHORT
                    )
                    savedToast.show()
                } else {
                    val unsavedToast = Toast.makeText(
                        applicationContext,
                        "Oops! Image could not be saved.", Toast.LENGTH_SHORT
                    )
                    unsavedToast.show()
                }
                drawBoard.destroyDrawingCache()
            }
            saveDialog.setNegativeButton(
                "Cancel"
            ) { dialog, which -> dialog.cancel() }
            saveDialog.show()
        }
    }

//    fun brushClicked(view: View, i: Boolean) {
//    }

    fun paintClicked(view: View) {
        var drawBoard = drawing
        var paintLayout = paint_colors
        var currPaint = paintLayout.getChildAt(0) as ImageButton
        currPaint.setImageDrawable(resources.getDrawable(R.drawable.paint_pressed))
        drawBoard.setErase(false)
        drawBoard.setBrushSize(drawBoard.getLastBrushSize())
        if(view != currPaint){
            val imgView = view as ImageButton
            val color = view.getTag().toString()
            drawBoard.setColor(color)
            imgView.setImageDrawable(resources.getDrawable(R.drawable.paint_pressed))
            currPaint.setImageDrawable(resources.getDrawable(R.drawable.paint))
            currPaint = view
        }
    }

    fun drawClick(view: View) {
        var brushDialog: Dialog
        var smallBrush = 10F
        var mediumBrush = 20F
        var largeBrush = 30F
        val drawBoard: DrawBoard = drawing
        val paintLayout = paint_colors
        brushDialog = Dialog(this)
        brushDialog.setTitle("Brush size:")
        brushDialog.setContentView(R.layout.brush_chooser)
        brushDialog.show()

        small_brush.setOnClickListener(){
            drawBoard.setBrushSize(smallBrush)
            drawBoard.setLastBrushSize(smallBrush)
            drawBoard.setErase(false)
            brushDialog.dismiss()
        }

        medium_brush.setOnClickListener(){
            drawBoard.setBrushSize(mediumBrush)
            drawBoard.setLastBrushSize(mediumBrush)
            drawBoard.setErase(false)
            brushDialog.dismiss()
        }

        large_brush.setOnClickListener(){
            drawBoard.setBrushSize(largeBrush)
            drawBoard.setLastBrushSize(largeBrush)
            drawBoard.setErase(false)
            brushDialog.dismiss()
        }
    }
    fun eraseClick(view: View) {
        var brushDialog: Dialog
        var smallBrush = 10F
        var mediumBrush = 20F
        var largeBrush = 30F
        val drawBoard: DrawBoard = drawing
        val paintLayout = paint_colors
        brushDialog = Dialog(this)
        brushDialog.setTitle("Eraser size:")
        brushDialog.setContentView(R.layout.brush_chooser)
        brushDialog.show()

        small_brush.setOnClickListener(){
            drawBoard.setErase(true)
            drawBoard.setBrushSize(smallBrush)
            brushDialog.dismiss()
        }

        medium_brush.setOnClickListener(){
            drawBoard.setErase(true)
            drawBoard.setBrushSize(smallBrush)
            brushDialog.dismiss()
        }

        large_brush.setOnClickListener(){
            drawBoard.setErase(true)
            drawBoard.setBrushSize(smallBrush)
            brushDialog.dismiss()
        }
    }


}